# Tribute to Linux

A little animation to celebrate Linux.

![](./assets/tux-linux-color.svg)

Why the colored "u"? Because, at the core, Linux is about:

* being free to choose
* owning your tech
* contributing to the community as a selfless act

It's all about *U* 💛



## TO DO

* [ ] Create alternative designs
* [ ] Create a plymouth theme
* [ ] Create a typeface to fit the design (Currently using [Urbanist](https://github.com/coreyhu/Urbanist))

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://gitlab.com/quazar-omega/tribute-to-linux">
    <span property="dct:title">Fabrizio Volonté</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Tribute to Linux</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="IT" about="https://gitlab.com/quazar-omega/tribute-to-linux">
  Italy</span>.
</p>
